import * as React from 'react';
import { Modal, Button, Form, Col, Tabs, Tab, InputGroup } from 'react-bootstrap';



type ObjectEditorProp = {
	data: any,
	onSave: any,
	onClose: any
}

type ObjectEditorState = {
	data: any,
	validated: boolean
}


export abstract class ObjectEditor extends React.Component<ObjectEditorProp,ObjectEditorState> {

   	constructor(props: ObjectEditorProp) {
     	super(props);
     	this.save = this.save.bind(this);
     	this.state = { data: this.getValue(props), validated: false };
   	}

   	componentWillReceiveProps(props: ObjectEditorProp) {
     	this.setState({ data: this.getValue(props), validated: false });
    	}

   	changeHandler(event: any) {
     	let data = this.state.data;
     	data[event.target.name] = event.target.value;
     	this.setState({ data: data });
   	}

   	save() {
     	this.props.onSave(this.state.data);
     	this.props.onClose();
   	}
   
   	submitHandler(event: any) {
    	event.preventDefault();
    	event.target.className += " was-validated";
   	}
   
	abstract getValue(props: ObjectEditorProp):any;
   
}
   
   
   
export class URLSourceEditor extends ObjectEditor {   

	static protocols: Array<string> = [ "ftp", "s3", "http", "https" ];
	static defaultValue: any = { protocol: "ftp", path: 'New', key: -1};
	

	
	getValue(props: ObjectEditorProp):any {
   		return (!props || !props.data) ? URLSourceEditor.defaultValue : props.data;
   	}
	

   	render() {
     	const data = this.state.data;
		console.log(data);

		return (
		   <Modal size="lg" show={true} onHide={this.props.onClose}>
			 <Modal.Header closeButton>
			   <Modal.Title>Connexion</Modal.Title>
			 </Modal.Header> 
			 <Modal.Body>
			   <Form noValidate validated={this.state.validated}  onSubmit={this.submitHandler} >

				 <Form.Group className="row" controlId="formName" >
				   <Form.Label column md={2} >Name</Form.Label>
				   <Col md="10">
					  <Form.Control required  type="text" value={data.name} name="name" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter name" />
				   </Col>
				 </Form.Group>
			 
				 <Form.Group className="row" controlId="formURL">
				   <Form.Label column md={2} >Local URL</Form.Label>
				   <Col md="10">
					 <InputGroup >
					   <InputGroup.Prepend >
						<InputGroup.Text >blue://</InputGroup.Text> 
					   </InputGroup.Prepend>
					   <Form.Control required  type="text" value={data.url} name="url" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter URL" />
					 </InputGroup>
				   </Col>
				 </Form.Group>

				 <Form.Group className="row" controlId="formDescription">
					<Form.Label column md={2}>Description</Form.Label>
					<Col md="10">
						<Form.Control as="textarea" value={data.description} name="description" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter description"rows="2" />
					</Col>
				 </Form.Group>
			 
				 <Form.Row>
				 <Form.Group className="col-md-2">
					 <Form.Label>Remote URL</Form.Label>
				 </Form.Group>
			 
				 <Form.Group className="col-md-2" controlId="formScheme">
				   <Form.Label>Scheme</Form.Label>
				   <Form.Control as="select" value={data.scheme} name="scheme" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter scheme" >
					 { URLSourceEditor.protocols.map( (value: string) => { return (<option>{value}</option>); } )}
				   </Form.Control>
				 </Form.Group>

				 <Form.Group className="col-md-6" controlId="formHost">
				   <Form.Label>Host</Form.Label>
				   <Form.Control type="text" value={data.host} name="host" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter host" />
				 </Form.Group>
			 
				 <Form.Group className="col-md-2" controlId="formPort">
				   <Form.Label>Port</Form.Label>
				   <Form.Control type="text" value={data.port} name="port" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter port" />
				 </Form.Group>
				  </Form.Row>

				 <Form.Group className="row" controlId="formPath">
				   <Form.Label column md="2" >Path</Form.Label>
				   <Col md="10">
				   <Form.Control type="text" value={data.path} name="path" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter path" />
				  </Col>
				 </Form.Group>
		 <Form.Group>
			<Tabs defaultActiveKey="credential" id="identityTab">
			   <Tab eventKey="credential" title="Credentials">
			   <Form.Row>
				 <Form.Group className="col-md-6" controlId="formUsername">
				   <Form.Label>Username</Form.Label>
				   <Form.Control type="text" value={data.username} name="username" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter username" />
				 </Form.Group>
				 <Form.Group  className="col-md-6" controlId="formPassword">
				   <Form.Label>Password</Form.Label>
				   <Form.Control type="password" value={data.password} name="password" onChange={(e:any) => this.changeHandler(e)} placeholder="Enter password" />
				 </Form.Group>
				 </Form.Row>
			   </Tab>
			   <Tab eventKey="certificate" title="Certificate">ddd
			   </Tab>
			</Tabs>
			 </Form.Group>
			   </Form>
			 </Modal.Body>
		   <Modal.Footer>
			 <Button variant="secondary" onClick={this.props.onClose}>Close</Button>
			 <Button variant="primary" onClick={this.save}>Save</Button>
		   </Modal.Footer>
		 </Modal>);
   }
   
}

