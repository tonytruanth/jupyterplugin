
import * as React from 'react';
import {BoxPanel } from '@phosphor/widgets';
import {JSONExt} from '@phosphor/coreutils';
import {JupyterFrontEndPlugin, ILayoutRestorer} from '@jupyterlab/application';
import {ICommandPalette, ReactWidget, WidgetTracker} from '@jupyterlab/apputils';
import {ResourceManager} from './ResourceManager';
import '../style/index.css';


class MyWrapperWidget extends ReactWidget {
  render() {
    return (<ResourceManager url="http://localhost:8080/data.json" fields="type,vendor,scope,scheme,name,host,port,tags" />);
  }
}


const extension: JupyterFrontEndPlugin<void> = {
  id: 'my-launcher',
  autoStart: true,
  requires: [ICommandPalette, ILayoutRestorer],
  activate: ( app, palette, restorer) => {

	let panel: BoxPanel;
  	const command: string = 'datasource:open';
  	 
  	app.commands.addCommand(command, {
  
    	label: 'Manage Data Sources',
    	execute: () => {
    	    if (!panel) {
    	    	const content = new MyWrapperWidget();
    			//const panel = new MainAreaWidget({content});
    			panel = new BoxPanel();
    			panel.id = 'datasources';
    			panel.title.label = 'Data Sources';
    			panel.title.closable = true;

   				panel.addWidget(content);
   			}
    		
    		if (!tracker.has(panel)) {
        		tracker.add(panel);
      		}
      		
      		if (!panel.isAttached) {
        		app.shell.add(panel,'main');
      		}	
    		
    		app.shell.activateById(panel.id);
    	}
    });
    
    palette.addItem({ command, category: 'Data Source Manager' });
    
    let tracker = new WidgetTracker<BoxPanel>({ namespace: 'datasources' });
  
  	restorer.restore(tracker, {
    	command,
    	args: () => JSONExt.emptyObject,
    	name: () => 'datasources'
  	});
    
  }
};


export default extension;
