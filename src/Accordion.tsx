import * as React from 'react';
//import { ObjectEditor} from './ObjectEditor';
import "../style/Accordion.css";


type AccordionProp = {
	data: Array<any>,
	editor: any,
	onClick: any,
	onDelete: any
}

type AccordionState = {
	data: Array<any>
}


export class Accordion extends React.Component<AccordionProp,AccordionState> {

	constructor(props: AccordionProp) {
    	super(props);
    	this.state = { data: props.data };
  	} 

  	render() {      
     	return (
         	<div className="accordion" >
 	        	{ Object.keys(this.props.data).map( (key: any,index: number) => 
	            	<AccordionSection key={key} title={key} data={this.props.data[key]} editor={this.props.editor}
                    	onDelete={this.props.onDelete} onClick={this.props.onClick} 
                	/>
            	)}     
         	</div>
     	);
  	}
  	
}



type AccordionSectionProp = {
	title: string,
	data:  Array<any>,
	editor: any,
	onClick: any,
	onDelete: any
}

type AccordionSectionState = {
	active: boolean
}

class AccordionSection extends React.Component<AccordionSectionProp,AccordionSectionState> {

	constructor(props: any) {
		super(props);
     	this.state = {active: true};
     	this.toggle = this.toggle.bind(this);
  	}

   	toggle(event: any) {
      	let state = this.state.active;
      	this.setState({ active: !state });
   	}

   	render() {
     	const style = { display: this.state.active  ? 'flex' : 'none' };
     	const classes = 'accordionHeader' + (this.state.active ? '':' active');
      
     	return (
		   <section className="accordionSection">
			 <button className={classes} onClick={this.toggle} >{this.props.title + ' (' + this.props.data.length  + ')'}</button>
			 <div className="accordionPanel" style={style} >
				{ this.props.data.map( (item: any,index: number) =>
				  <AccordionElement key={item.key} data={item} editor={this.props.editor[item.type].editor}
					 onDelete={this.props.onDelete} 
					 onClick={(e:any) => this.props.onClick(item) } 
				  />
				)} 
			 </div>
		   </section>
     	);
   	}
   	
}





type AccordionElementProp = {
	data: any,
	editor: any,
	onClick: any,
	onDelete: any	
}

class AccordionElement extends React.Component<AccordionElementProp,{}> {
	
	render() {  
  		const icon: string = this.props.editor["getIcon64"](this.props.data); 	
  	
    	return (
      		<div className="accordionIcon" onClick={this.props.onClick} >    
          		<img src={ "data:image/png;base64," + icon } />
         		{ this.props.onDelete &&  <button className="delete" onClick={(e:any) => this.props.onDelete(this.props.data.key,e)} >x</button> }
         		<p>{this.props.data.name}</p>
      		</div>
    	);
	}
}




