
import { JupyterFrontEnd, JupyterFrontEndPlugin, ILayoutRestorer} from '@jupyterlab/application';
import {ICommandPalette, MainAreaWidget, WidgetTracker} from '@jupyterlab/apputils';
import {JSONExt} from '@phosphor/coreutils';
import '../style/index.css';

import MyWidget from './myComponent';


function activate(app: JupyterFrontEnd, palette: ICommandPalette, restorer: ILayoutRestorer) {

  console.log('JupyterLab extension jupyterlab_xkcd is activated!');

  let content;
  let widget: MainAreaWidget<MyWidget>;
  const command: string = 'xkcd:open';
  
  app.commands.addCommand(command, {
    label: 'Pipeline',
    execute: () => {
      if (!widget) {
        content = new MyWidget();
        widget = new MainAreaWidget({content});
      }
      
      if (!tracker.has(widget)) {
        tracker.add(widget);
      }
      if (!widget.isAttached) {
        app.shell.add(widget,'main');
      }
        
      widget.content.update();
      app.shell.activateById(widget.id);
    }
  });

  palette.addItem({ command, category: 'Tutorial' });


let tracker = new WidgetTracker<MainAreaWidget<MyWidget>>({ namespace: 'pipeline' });
  
  restorer.restore(tracker, {
    command,
    args: () => JSONExt.emptyObject,
    name: () => 'pipeline'
  });
};



const extension: JupyterFrontEndPlugin<void> = {
  id: 'jupyterlab_pipeline',
  autoStart: true,
  requires: [ICommandPalette, ILayoutRestorer],
  activate: activate
};

export default extension;
