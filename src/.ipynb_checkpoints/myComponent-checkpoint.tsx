import * as React from 'react';
//import { ReactWidget } from '@jupyterlab/apputils';
import { Widget} from '@phosphor/widgets';


function MyComponent() {
  return <div>My Widget</div>;
}

export default class MyWidget extends Widget {

  render() {
    return <MyComponent />;
  }
  
}


