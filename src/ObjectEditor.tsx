import * as React from 'react';


export type ObjectEditorProp = {
	data: any,
	onSave: any,
	onClose: any
}

export type ObjectEditorState = {
	data: any,
	validated: boolean
}


/*export interface IObjectEditor {

	getIcon64(data: any):string;
	getValue(props: ObjectEditorProp):

}*/

export abstract class ObjectEditor extends React.Component<ObjectEditorProp,ObjectEditorState> {

   	constructor(props: ObjectEditorProp) {
     	super(props);
     	this.save = this.save.bind(this);
     	this.state = { data: this.getValue(props), validated: false };  	
   	}

   	componentWillReceiveProps(props: ObjectEditorProp) {
     	this.setState({ data: this.getValue(props), validated: false });
    }

   	changeHandler(event: any) {
     	let data = this.state.data;
     	data[event.target.name] = event.target.value;
     	this.setState({ data: data });
   	}

   	save() {
     	this.props.onSave(this.state.data);
     	this.props.onClose();
   	}
   
   	submitHandler(event: any) {
    	event.preventDefault();
    	event.target.className += " was-validated";
   	}
   
   	static getIcon64(data: any):string { return ""; }
   	
	abstract getValue(props: ObjectEditorProp):any;
   
}
   
   