import * as React from 'react';


type ModalProviderProp = {
	onSave: (item: any) => void
}

type ModalProviderState = {
	component: any,
	props: any,
	showModal: (component: any, props?: any) => void,
	hideModal: () => void,
	saveModal: (item: any) => void
}


export const ModalContext = React.createContext<ModalProviderState>({
  	component: null,
  	props: {},
  	showModal: (component: any, props?: any) => {},
  	hideModal: () => {},
	saveModal: (item: any) => {}
});


export class ModalProvider extends React.Component<ModalProviderProp,ModalProviderState> {

  	showModal = (component: any, props?: any) => {    
		this.setState({
      		component,
      		props
    	});
  	}

  	hideModal = () => {
		this.setState({
    		component: null,
    		props: {},
  		});
  	}
  
	constructor(props: ModalProviderProp) {
  		super(props);
  		this.state = {
    		component: null,
    		props: {},
    		showModal: this.showModal,
    		hideModal: this.hideModal,
    		saveModal: props.onSave
  		};	
  	}

  	render() {
    	return (
      		<ModalContext.Provider value={this.state}>{this.props.children}</ModalContext.Provider>
    	);
  	}
  
}

export const ModalConsumer = ModalContext.Consumer;


export const ModalRoot = () => {

	return  (
  		<ModalContext.Consumer>
    		{ ( { component, props, hideModal, saveModal} ) => {;
    			const Component = component;
      			return Component ? <Component {...props} onClose={hideModal} onSave={saveModal} /> : null;
   			}}
  		</ModalContext.Consumer>
  	);
}

