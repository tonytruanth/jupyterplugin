import * as React from 'react';
import {Spinner} from 'react-bootstrap';
import {Accordion} from './Accordion';
import {DatabaseEditor} from './DatabaseEditor';
import {URLSourceEditor} from './URLSourceEditor';
import {StreamSourceEditor} from './StreamSourceEditor';
import { Button, DropdownButton, Dropdown, InputGroup, FormControl} from 'react-bootstrap';
import '../style/ResourceManager.css';
import '../style/bootstrap.min.css';

import {ModalProvider,ModalConsumer,ModalRoot} from './ModalProvider';


const editors: any = {
	url: { name: "URL Source", editor: URLSourceEditor},
	database: { name: "Database Source", editor: DatabaseEditor},
	stream: { name: "Stream Source", editor: StreamSourceEditor}
};


type ResourceManagerProps = {
	url: string,
	fields: string
}

type ResourceManagerState = {
	data: Array<any>,
	pattern: string,
	isLoading: boolean,
	fields: Array<string>,
	selectedOption: string
}


export class ResourceManager extends React.Component<ResourceManagerProps,ResourceManagerState> {

  url: string = "http://localhost:8080/data.json";
  
  
  constructor(props: ResourceManagerProps) {
    super(props);
    this.save = this.save.bind(this);
    this.group = this.group.bind(this);
    this.select = this.select.bind(this);
    this.delete = this.delete.bind(this);
    this.refresh = this.refresh.bind(this);
    
    const fields = props.fields.split(',');
    
    this.state = { 
    	data: [],
     	pattern: ".*",
     	fields: fields,
     	isLoading: false,
     	selectedOption: fields[0]
    };
    
  } 
  
  componentDidMount() {
  	this.refresh();
  }
  
  group(by: string,search: string) {
	let regex = new RegExp(this.state.pattern,"i");
  
   	return this.state.data
   		.filter( (item: any,index: number) => item[search].match(regex) )
   		.reduce( (newData: any, item: any, index: number) => {
       		if ( !newData[item[by]] ) newData[item[by]] = [];
       		item['key'] = index;
       		newData[item[this.state.selectedOption]].push(item); 
       		return newData;
     	}, {});
  }
  
  
  select(eventKey: any, event: any) {
	 this.setState({ 
	 	selectedOption: this.state.fields[eventKey] 
	 });
  }
  
  
  refresh() {
	this.setState( { isLoading: true} );
    
    fetch(this.props.url) 
		.then( res => { return res.json() } )
		.then( json => { this.setState( { data: json, isLoading: false} ) } )
		.catch( error => { console.log(error) } )
  }


  delete(index: number, e: any) {
    let data = this.state.data;
    data.splice(index, 1);
    this.setState({ data: data });
    e.stopPropagation();
  }
  
  
  save(item: any) {
    let data = this.state.data;
    if ( item.key >= 0 ) {
      data[item.key] = item;
    } else {
      data.push(item);
    }
    this.setState( { data: data});    
  }
  
  
  render() {
  	
  	let data = this.group(this.state.selectedOption,'name');
  	
  	if ( this.state.isLoading ) {  return (<Spinner animation="border" />); }
  
    return (
    	<ModalProvider onSave={this.save} >
    		<ModalRoot />
    		<ModalConsumer>
    			{({ showModal }) => (
					<div className="App">
						<div>
							<InputGroup size="sm" >
								<InputGroup.Prepend>
								   <InputGroup.Text >Group By</InputGroup.Text>
								   <DropdownButton size="sm" as={InputGroup.Append} id="inputGroupBy" variant="secondary"
									  title={this.state.selectedOption} onSelect={this.select}
								   >
									  { this.state.fields.map((opt:any,index:any) => (<Dropdown.Item key={index} eventKey={index}>{opt}</Dropdown.Item >) )}
								   </DropdownButton>
								</InputGroup.Prepend>
								<FormControl size="sm" placeholder="Search criteria" aria-describedby="basic-addon2"
									onChange={(e:any) => this.setState( { pattern: e.target.value} )}
								/>
								<Button size="sm" variant="secondary" onClick={this.refresh} >Refresh</Button>
								<DropdownButton size="sm" variant="primary" title="New Data Source" id="newDataSource" >
								{ Object.keys(editors).map( (key: string, index: any) => {
									const value: any = editors[key];
									return (<Dropdown.Item size="sm" eventKey={index} onClick={ () => showModal(value.editor) } >{value.name}</Dropdown.Item>);
								})}
								</DropdownButton>
							</InputGroup> 
						</div>
						<Accordion data={data} editor={editors} onClick={ (item:any) => showModal( editors[item.type].editor, { data: item}) } onDelete={this.delete} /> 
					</div>
				)}
			</ModalConsumer>
		</ModalProvider>
	);
  }
  
  
}

