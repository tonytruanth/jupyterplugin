# pipelineExt

Pipeline extension


## Prerequisites

* JupyterLab

## Installation

```bash
> jupyter labextension install pipelineExt
```

## Development

For a development install (requires npm version 4 or later), do the following in the repository directory:

```bash
> npm install
> npm run build
> jupyter labextension link .
```

To rebuild the package and the JupyterLab app:

```bash
npm run build
> jupyter lab build
```

# Execution 

Launch HTTP_server on port 3333 (this server simulates the service managing the data) 

```bash
> http-server -p 3333 --cors
```

in a browser test the server wit http://localhost:3333/data.json

Launch Jupyter Lab with pipenv

```bash
> pipenv shell
> jupyter lab
```

Jupyter Lab will be accessible at the following URL:

```bash
http://http://localhost:8888/lab
```

In the Palette, you find a menu "Data Source Manager" allowing to display the Datasource Manager

![Alt text](./images/datasourceMGR_screen.png)

![Alt text](./images/datasourceMGR_detail.png)




